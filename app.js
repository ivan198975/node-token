const express = require('express');
const app = express();
const rotaprodutos = require('./routes/produtos');

app.use('/produtos', rotaprodutos);

module.exports = app;