const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).send({
        mensagem: 'usando get dentro de produtos'
    });
});


router.post('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'usando post dentro de produtos'
    });
});

router.get('/:id_produto', (req, res, next) => {
    const id = req.params.id_produto

        if (id ===  'especial') {
            res.status(200).send({
                mensagem: 'voce descobriu o ID especial',
                id: id
            });
        } else {
            res.status(200).send({
                mensagem: 'voce passou o ID'
            });
        }
});

module.exports = router;